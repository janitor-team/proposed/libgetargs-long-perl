libgetargs-long-perl (1.1012-3) unstable; urgency=medium

  [ Colin Watson ]
  * Use debhelper-compat instead of debian/compat.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

 -- Colin Watson <cjwatson@debian.org>  Sat, 06 Feb 2021 17:41:12 +0000

libgetargs-long-perl (1.1012-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field

  [ Colin Watson ]
  * Set Rules-Requires-Root: no.

 -- Colin Watson <cjwatson@debian.org>  Sun, 10 Feb 2019 19:23:56 +0000

libgetargs-long-perl (1.1012-1) unstable; urgency=medium

  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Fri, 13 Jul 2018 00:24:18 +0100

libgetargs-long-perl (1.1011-1) unstable; urgency=medium

  * New upstream release.
  * debian/watch: Drop uupdate.

 -- Colin Watson <cjwatson@debian.org>  Tue, 10 Jul 2018 12:41:24 +0100

libgetargs-long-perl (1.1010-1) unstable; urgency=medium

  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Sat, 07 Jul 2018 09:07:44 +0100

libgetargs-long-perl (1.1008-1) unstable; urgency=medium

  * New upstream release.
  * Move VCS to salsa.debian.org.
  * Update search.cpan.org URLs to metacpan.org.

 -- Colin Watson <cjwatson@debian.org>  Fri, 06 Jul 2018 13:18:36 +0100

libgetargs-long-perl (1.1007-3) unstable; urgency=medium

  * Convert debian/copyright to copyright-format 1.0.
  * Upgrade to debhelper v9.
  * Use HTTPS for Vcs-* URLs.

 -- Colin Watson <cjwatson@debian.org>  Wed, 27 Jan 2016 12:53:58 +0000

libgetargs-long-perl (1.1007-2) unstable; urgency=medium

  * Mark package as autopkgtestable.

 -- Colin Watson <cjwatson@debian.org>  Thu, 24 Dec 2015 01:22:19 +0000

libgetargs-long-perl (1.1007-1) unstable; urgency=medium

  * New upstream release.
  * Build-depend on liburi-perl for the sake of the bundled
    Module::Install::Bugtracker module.

 -- Colin Watson <cjwatson@debian.org>  Fri, 01 May 2015 10:05:46 +0100

libgetargs-long-perl (1.1005-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-Browser URL for alioth cgit.
  * Build-depend on libtest-pod-perl for the new POD test.
  * Policy version 3.9.6: no changes required.

 -- Colin Watson <cjwatson@debian.org>  Tue, 24 Mar 2015 10:45:59 +0000

libgetargs-long-perl (1.1003-3) unstable; urgency=medium

  * Switch to git; add Vcs-* fields.
  * Add a Homepage field.
  * Policy version 3.9.5: refer to GPL-1 in copyright file rather than
    unversioned GPL.

 -- Colin Watson <cjwatson@debian.org>  Sun, 19 Jan 2014 18:01:15 +0000

libgetargs-long-perl (1.1003-2) unstable; urgency=low

  * Convert to source format 3.0 (quilt).
  * Policy version 3.8.4:
    - No longer need to build-depend on perl (>= 5.6.0-16).

 -- Colin Watson <cjwatson@debian.org>  Sun, 07 Mar 2010 23:13:33 +0000

libgetargs-long-perl (1.1003-1) unstable; urgency=low

  * New upstream release.
  * Convert to debhelper 7.
  * Policy version 3.8.2: no changes required.

 -- Colin Watson <cjwatson@debian.org>  Tue, 28 Jul 2009 09:44:52 +0100

libgetargs-long-perl (1.1002-1) unstable; urgency=low

  * New upstream release.
  * Upstream relicensed under Perl's licensing terms (more liberal than the
    previous GPL-only status). Update debian/copyright.
  * Build-depend on libmodule-install-perl (>= 0.40), since
    Module::AutoInstall is missing from inc/.

 -- Colin Watson <cjwatson@debian.org>  Tue, 01 Apr 2008 12:37:34 +0100

libgetargs-long-perl (1.1001-4) unstable; urgency=low

  * Fix watch file format.

 -- Colin Watson <cjwatson@debian.org>  Tue, 29 Jan 2008 01:23:31 +0000

libgetargs-long-perl (1.1001-3) unstable; urgency=low

  * debian/watch: use dist-based URL.
  * debian/copyright: Adjust to point to GPLv2 specifically.
  * Policy version 3.7.3: no changes required.
  * Use debhelper v5.
  * Don't ignore errors from 'make distclean' other than the Makefile not
    existing.
  * Remove /usr/lib/perl5 in a more standard way.

 -- Colin Watson <cjwatson@debian.org>  Tue, 29 Jan 2008 01:19:44 +0000

libgetargs-long-perl (1.1001-2) unstable; urgency=low

  * Move everything from Build-Depends-Indep to Build-Depends; it's all used
    in the clean target.
  * Policy version 3.7.2.

 -- Colin Watson <cjwatson@debian.org>  Sun,  2 Jul 2006 23:11:26 +0100

libgetargs-long-perl (1.1001-1) unstable; urgency=low

  * New upstream release (closes: #329550).
  * Policy version 3.6.2: no changes required.
  * Upgrade to debhelper v4.

 -- Colin Watson <cjwatson@debian.org>  Thu, 22 Sep 2005 09:43:26 +0100

libgetargs-long-perl (1.1000-2) unstable; urgency=low

  * Set section to 'perl' to match override file.
  * Build-depend on liblog-agent-perl (>= 0.105); thanks, Daniel Holbach.

 -- Colin Watson <cjwatson@debian.org>  Wed,  1 Jun 2005 22:54:01 +0100

libgetargs-long-perl (1.1000-1) unstable; urgency=low

  * New upstream release.
  * Licence changed to GPL. Update debian/copyright.
  * Policy version 3.6.1. No changes required.

 -- Colin Watson <cjwatson@debian.org>  Thu, 26 May 2005 16:06:24 +0100

libgetargs-long-perl (0.1.3-3) unstable; urgency=low

  * Policy version 3.5.7: shift build rules to build-indep target.

 -- Colin Watson <cjwatson@debian.org>  Sat,  5 Oct 2002 13:32:23 +0100

libgetargs-long-perl (0.1.3-2) unstable; urgency=low

  * Use Build-Depends-Indep rather than Build-Depends.
  * Policy version 3.5.6.
  * Perl policy 1.20 (debian/rules tweaks, tighten debhelper
    build-dependency).

 -- Colin Watson <cjwatson@debian.org>  Wed, 19 Dec 2001 20:03:23 +0000

libgetargs-long-perl (0.1.3-1) unstable; urgency=low

  * Initial release (closes: #102394).
  * Hardcoded dependency on perl (>= 5.6.1); apparently Getargs::Long
    exercises a bug in 5.6.0.

 -- Colin Watson <cjwatson@debian.org>  Tue, 26 Jun 2001 20:06:41 +0100
